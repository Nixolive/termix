FROM golang:1.13.4 as builder

COPY go.mod go.sum /go/src/gitlab.com/Nixolive/termix/
WORKDIR /go/src/gitlab.com/Nixolive/termix
RUN go mod download

COPY . /go/src/gitlab.com/Nixolive/termix/
