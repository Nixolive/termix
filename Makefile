.PHONY: default
default: lint test

synchronizemod:
	go mod tidy
	go list -m all

.PHONY: lint
lint:
	GOGC=50 golangci-lint run

.PHONY: test
test:
	echo 'mode: atomic' > coverage.txt ; go list ./... | xargs -n1 -I{} sh -c 'go test -v -short -bench=. -covermode=atomic -coverprofile=coverage.tmp {} ; tail -n +2 coverage.tmp >> coverage.txt' ; rm coverage.tmp
	go list ./... | xargs -n1 -I{} sh -c 'go test -v -short -bench=. -covermode=atomic -coverprofile=coverage.tmp {} ; tail -n +2 coverage.tmp >> coverage.txt' ; rm coverage.tmp

.PHONY: build
build:
	CGO_ENABLED=0 GOOS=linux go build -o build/termix gitlab.com/Nixolive/termix/cmd/termix

.PHONY: start
start:
	go run ./cmd/termix

.PHONY: clean
clean:
	rm -rf build

.PHONY: install-lint
install-lint:
	# In alpine linux (as it does not come with curl by default)
	# wget -O - -q https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s v1.21.0
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s -- -b $(GOPATH)/bin v1.21.0

.PHONY: install-linters
install-linters:
	# binary will be $(go env GOPATH)/bin/golangci-lint
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s -- -b $(GOPATH)/bin v1.21.0
	python3 -m pip install --user --upgrade pip
	python3 -m pip install --user pre-commit
	python3 -m pip install --user gitlint
	pre-commit install -f
	pre-commit install --hook-type commit-msg
	pre-commit autoupdate

.PHONY: uninstall-commit
uninstall-commit:
	pre-commit uninstall 
	gitlint uninstall
